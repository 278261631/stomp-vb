using NUnit.Framework;
using Stomp.Client;

namespace Stomp.Test
{
    [TestFixture]
    public class AcceptanceTest
    {
        [Test]
        public void SendAndSyncReceive()
        {
            using (StompConnection stomp = new StompConnection("msg.xtelescope.net", 61613))
            {
                stomp.Subscribe("/queue/test");
                stomp.Send("/queue/test", "Hello world!");
                Message message = stomp.WaitForMessage();
                Assert.AreEqual("Hello world!", message.Body);
            }
        }


        public static void Main(string[] args)
        {
            using (StompConnection stomp = new StompConnection("msg.xtelescope.net", 61613))
            {
                stomp.Subscribe("/queue/test");
                stomp.Send("/queue/test", "Hello world!");
                Message message = stomp.WaitForMessage();
                Assert.AreEqual("Hello world!", message.Body);
            }
        }
    }
}
